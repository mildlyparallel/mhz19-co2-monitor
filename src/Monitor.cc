#include <fstream>
#include <cassert>
#include <iostream>
#include <stdexcept>
#include <system_error>
#include <chrono>
#include <thread>
#include <cmath>

#include <signal.h>

#include "Monitor.hh"
#include "Sensor.hh"
#include "Output.hh"

#include "config.h"

#define STR_HELPER(x) #x
#define STR(x) STR_HELPER(x)

#define THROW_IF(cond) {\
	if ((cond)) \
		throw std::system_error(errno, std::generic_category(), __FILE__ ":" STR(__LINE__)); \
} while(false);

static bool stopped = false;

static void exit_handler(int)
{
	stopped = true;
}

void Monitor::set_exit_handler()
{
	struct sigaction sact = {};
	sact.sa_flags = 0;
	sact.sa_handler = exit_handler;

	int rc = sigemptyset(&sact.sa_mask);
	THROW_IF(rc < 0);

	int signals[] = {SIGINT, SIGHUP, SIGTERM, SIGQUIT};
	for (auto s : signals) {
		rc = sigaddset(&sact.sa_mask, s);
		THROW_IF(rc < 0);
	}

	for (auto s : signals) {
		rc = sigaction(s, &sact, nullptr);
		THROW_IF(rc < 0);
	}
}

Monitor::Monitor()
{  }

Monitor::~Monitor()
{  }

void Monitor::set_output(Output *output)
{
	m_output = output;
}

void Monitor::loop()
{
	assert(m_output);

	Sensor sensor;

	sensor.open();

	while (!stopped) {
		uint64_t ts = std::chrono::duration_cast<std::chrono::milliseconds>(
			std::chrono::system_clock::now().time_since_epoch()
		).count();

		unsigned co2 = sensor.read();

		push(ts, co2);

		std::this_thread::sleep_for(std::chrono::milliseconds(UPDATE_DELAY_MS));
	}

	sensor.close();
}

bool Monitor::check_sample_delta(size_t id) const
{
	static const size_t n = SAMPLE_INTERP_DEGREE + 1;

	double x = m_buffer[id].first;
	double y = m_buffer[id].second;

	double xx[n];
	size_t ii[n];
	for (size_t i = 0; i < n; ++i) {
		ii[i] = i * (m_buffer.size() - 1) / (n - 1);
		xx[i] = m_buffer[ii[i]].first;
	}

	double L = 0;
	for (size_t i = 0; i < n; ++i) {
		if (ii[i] == id)
			return true;

		double l = 1;

		for (size_t j = 0; j < n; ++j) {
			if (i == j)
				continue;

			l *= (x - xx[j]) / (xx[i] - xx[j]);
		}

		L += m_buffer[ii[i]].second * l;
	}

	double delta = std::fabs(L - y);
	return delta > SAMPLE_DELTA_THRESHOLD;
}

void Monitor::push(uint64_t ts, unsigned co2)
{
	m_output->store_current(ts, co2);

	m_buffer[m_buffer_pos] = {ts, co2};
	m_buffer_pos++;

	if (m_buffer_pos < m_buffer.size())
		return;

	for (size_t i = 0; i < m_buffer.size(); ++i) {
		if (check_sample_delta(i))
			m_output->store(m_buffer[i].first, m_buffer[i].second);
	}

	m_buffer_pos = 0;
}

