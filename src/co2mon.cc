#include <iostream>
#include <memory>

#include "Monitor.hh"
#include "OutputSqlite.hh"
#include "OutputStdout.hh"
#include "config.h"

int main(int argc, char *argv[])
{
	std::string output_uri(OUTPUT_URI);
	if (argc > 1)
		output_uri = argv[1];

	Monitor::set_exit_handler();

#ifdef DUMMY_SENSOR
	std::cout << "Using dummy sensor"  << std::endl;
#else
	std::cout << "Reading sensor " << MHZ19_DEVICE << std::endl;
#endif

	std::unique_ptr<Output> output;

	if (output_uri == "-") {
		output = std::make_unique<OutputStdout>();
		std::cout << "Writing to stdout" << std::endl;
	} else {
		output = std::make_unique<OutputSqlite>();
		output->open(output_uri);
		std::cout << "Writing to " << output_uri << std::endl;
	}

	Monitor monitor;
	monitor.set_output(output.get());

	std::cout << "Reading sensor data" << std::endl;
	monitor.loop();

	std::cout << "\nExeting\n";

	return 0;
}

