#include "SqliteException.hh"

#include <sqlite3.h>

SqliteException::SqliteException(int rc)
: m_code(rc)
{ }

const char *SqliteException::what() const noexcept {
	return sqlite3_errstr(m_code);
}


