#include <cassert>

#include "OutputSqlite.hh"

#include "Sqlite.hh"
#include "SqliteException.hh"
#include "SqliteStatement.hh"

static const constexpr char *create_record_table_sql =
	"CREATE TABLE IF NOT EXISTS Records ( "
		"timestamp INTEGER PRIMARY KEY, "
		"co2 INTEGER "
	");";

static const constexpr char *create_current_table_sql =
	"DROP TABLE IF EXISTS Current;"
	"CREATE TABLE Current ( "
		"timestamp INTEGER, "
		"co2 INTEGER "
	");"
	"INSERT INTO Current (timestamp) VALUES(0);";

static const constexpr char *insert_record_sql =
	"INSERT INTO Records (timestamp, co2) VALUES (?, ?);";

static const constexpr char *update_current_sql =
	"UPDATE Current SET "
		"timestamp = ?, co2 = ? "
	"WHERE rowid = 1;";

OutputSqlite::OutputSqlite()
{  }

OutputSqlite::~OutputSqlite()
{ }

void OutputSqlite::open(const std::string &uri)
{
	m_db.open(uri);

	m_db.exec(create_record_table_sql);
	m_db.exec(create_current_table_sql);
}

void OutputSqlite::store_current(uint64_t ts, uint64_t co2)
{
	SqliteStatement stmt(m_db, update_current_sql);
	stmt.bind_to(0, ts);
	stmt.bind_to(1, co2);
	stmt.step();
}

void OutputSqlite::store(uint64_t ts, uint64_t co2)
{
	SqliteStatement stmt(m_db, insert_record_sql);
	stmt.bind_to(0, ts);
	stmt.bind_to(1, co2);
	stmt.step();
}

void OutputSqlite::close()
{
	m_db.close();
}

