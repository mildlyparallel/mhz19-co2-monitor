#include <cassert>
#include <iostream>
#include <cstring>

#include <sqlite3.h>

#include "Sqlite.hh"
#include "SqliteException.hh"

Sqlite::Sqlite()
{ }

Sqlite::~Sqlite()
{
	if (m_handle)
		sqlite3_close(m_handle);
}

void Sqlite::open(const std::string &uri)
{
	assert(!uri.empty());
	assert(!m_handle);

	int rc = sqlite3_open(uri.c_str(), &m_handle);

	if (rc != SQLITE_OK)
		throw SqliteException(sqlite3_errcode(m_handle));
}

sqlite3 *Sqlite::get_handle()
{
	return m_handle;
}

uint64_t Sqlite::get_rowid() const
{
	return sqlite3_last_insert_rowid(m_handle);
}

void Sqlite::close()
{
	if (!m_handle)
		return;

	sqlite3_close(m_handle);
	m_handle = nullptr;
}

void Sqlite::exec(const char *sql)
{
	assert(m_handle);
	assert(sql);

	char *err = nullptr;

	int rc = sqlite3_exec(m_handle, sql, nullptr, 0, &err);

	if (rc != SQLITE_OK) {
		std::cerr << "sqlite3 error #" << sqlite3_errcode(m_handle) << ": " << err << std::endl;
		sqlite3_free(err);
		throw SqliteException(rc);
	}
}

