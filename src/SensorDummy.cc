#include <cassert>
#include <stdexcept>

#include "SensorDummy.hh"

void SensorDummy::open()
{ }

void SensorDummy::close()
{ }

unsigned SensorDummy::read()
{
	std::uniform_int_distribution<int> co2_distr(-3, 5);

	int c = co2_distr(m_gen);

	if (c < 0 && (int)m_co2 < -c)
		m_co2 = 0;
	else if (c > 0 && m_co2 >= 5000)
		m_co2 = 0;
	else
		m_co2 += c;

	return m_co2;
}


