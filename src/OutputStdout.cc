#include <cassert>
#include <iostream>

#include "OutputStdout.hh"

OutputStdout::OutputStdout()
{  }

OutputStdout::~OutputStdout()
{ }

void OutputStdout::open(const std::string &)
{ }

void OutputStdout::store(uint64_t, uint64_t)
{ }

void OutputStdout::close()
{ }

void OutputStdout::store_current(uint64_t, uint64_t co2)
{
	std::cout << "CO2[ppm]: " << co2 << std::endl;
}
