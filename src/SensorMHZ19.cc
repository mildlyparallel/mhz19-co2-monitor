#include <cassert>
#include <stdexcept>

#include <errno.h>
#include <fcntl.h>
#include <termios.h>
#include <system_error>
#include <unistd.h>

#include "SensorMHZ19.hh"

#define STR_HELPER(x) #x
#define STR(x) STR_HELPER(x)

#define THROW_IF(cond) {\
	if ((cond)) \
		throw std::system_error(errno, std::generic_category(), __FILE__ ":" STR(__LINE__)); \
} while(false);

SensorMHZ19::SensorMHZ19()
{  }

SensorMHZ19::SensorMHZ19(SensorMHZ19 &&other)
{
	if (opened())
		close();

	m_fd = other.m_fd;
	other.m_fd = -1;
}

SensorMHZ19::~SensorMHZ19()
{
	if (!opened())
		return;

	::close(m_fd);
}

void SensorMHZ19::open()
{
	assert(!opened());

	m_fd = ::open(c_device, O_RDWR | O_NOCTTY | O_SYNC);
	THROW_IF(m_fd < 0);

	set_tty_attr();
}

void SensorMHZ19::close()
{
	if (m_fd < 0)
		return;

	int rc = ::close(m_fd);
	THROW_IF(rc < 0);

	m_fd = -1;
}

bool SensorMHZ19::opened() const
{
	return m_fd >= 0;
}

void SensorMHZ19::set_tty_attr()
{
	assert(opened());
	// https://stackoverflow.com/questions/6947413/how-to-open-read-and-write-from-serial-port-in-c

	int rc = 0;
	struct termios tty;
	rc = ::tcgetattr(m_fd, &tty);
	THROW_IF(rc < 0);

	::cfsetospeed(&tty, B9600);
	::cfsetispeed(&tty, B9600);

	// 8-bit chars
	tty.c_cflag = (tty.c_cflag & ~CSIZE) | CS8;

	// disable IGNBRK for mismatched speed tests; otherwise receive break
	// as \000 chars
	// disable break processing
	tty.c_iflag &= ~IGNBRK;
	// no signaling chars, no echo,
	tty.c_lflag = 0;
	// no canonical processing
	// no remapping, no delays
	tty.c_oflag = 0;
	// read doesn't block
	tty.c_cc[VMIN]  = 0;
	// 0.5 seconds read timeout
	tty.c_cc[VTIME] = 5;
	// shut off xon/xoff ctrl
	tty.c_iflag &= ~(IXON | IXOFF | IXANY);

	// ignore modem controls,
	tty.c_cflag |= (CLOCAL | CREAD);
	// enable reading
	// shut off parity
	tty.c_cflag &= ~(PARENB | PARODD);
	tty.c_cflag |= 0;
	tty.c_cflag &= ~CSTOPB;
	tty.c_cflag &= ~CRTSCTS;

	tty.c_cc[VMIN] = 0;

	// 0.5 seconds read timeout
	tty.c_cc[VTIME] = 5;

	rc = ::tcsetattr(m_fd, TCSANOW, &tty);
	THROW_IF(rc < 0);
}

unsigned SensorMHZ19::read()
{
	assert(opened());

	ssize_t rc = 0;

	static const uint8_t cmd[] = {0xff, 0x01, 0x86, 0x00, 0x00, 0x00, 0x00, 0x00, 0x79};

	for (size_t i = 0; i < c_retries; ++i) {
		rc = ::write(m_fd, cmd, 9);
		THROW_IF(rc != 9);

		::usleep((9 + 25) * 100);

		uint8_t buf[9] = {0};
		rc = ::read(m_fd, buf, 9);
		THROW_IF(rc < 0);

		if (buf[0] != 0xff || buf[1] != 0x86)
			continue;

		unsigned co2 = static_cast<unsigned>(buf[2]) * 256 + buf[3];
		return co2;
	}

	throw std::runtime_error("Failed to read CO2 value after several attepts");
}

