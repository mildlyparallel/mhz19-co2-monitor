# set terminal canvas name "pl_recent_day" size 1000,400 lw 1.6
set terminal svg size 900,350
set output "public/recent-day.svg"

set xdata time
set timefmt "%s"
set format x "%H:%M"
set ylabel "CO2 [ppm]"
set yr [400:]
set xr [0:86400]
set title "Last days"

set datafile separator ","

plot \
	"recent-day-0.csv" using 1:2 with lines title "Today", \
	"recent-day-1.csv" using 1:2 with lines title "1 day ago"

