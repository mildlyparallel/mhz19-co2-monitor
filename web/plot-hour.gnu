# set terminal canvas name "pl_recent" size 1000,400
set terminal svg size 900,350
set output "public/recent-hour.svg"

set xdata time
set timefmt "%s"
set format x "%H:%M"
set ylabel "CO2 [ppm]"
set title "Last 3 hours"

set datafile separator ","
plot "recent-hour.csv" using 1:2 with lines notitle

