#!/bin/bash
db="${1:-output.db}"
out="public/current.js"

data="$(cat get-current-state.sql | sqlite3 "$db" | tr -d '\r')"

ts="$(echo $data | cut -d, -f1)"
co2="$(echo $data | cut -d, -f2)"

printf "var current_str = \'[{\"timestamp\":\"%s\",\"co2\":%s}]\';\n" $ts $co2 > $out


