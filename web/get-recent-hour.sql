.headers on
.mode csv

SELECT 
	strftime('%s', datetime(timestamp/1000, 'unixepoch'), 'localtime') AS ts,
	co2
FROM Records
WHERE timestamp > (strftime('%s', 'now', '-{hours} hours') * 1000)
ORDER BY timestamp ASC;

