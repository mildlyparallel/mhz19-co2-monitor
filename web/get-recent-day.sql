.headers on
.mode csv

SELECT 
	strftime('%s', datetime(timestamp/1000, 'unixepoch'), 'localtime') -
	(strftime('%s', 'now', 'start of day') - {day}*24*60*60) AS sec_since_day,
	co2
FROM Records
WHERE
	-- strftime('%s', 'now', 'start of day', 'localtime') would add 3 hours for
	-- some reason instead of subtracting 
	timestamp > (strftime('%s', 'now', 'start of day', '-3 hours') - {day}*24*60*60) * 1000
	AND
	timestamp < (strftime('%s', 'now', 'start of day', '-3 hours') - ({day}-1)*24*60*60 ) * 1000
ORDER BY timestamp ASC;

