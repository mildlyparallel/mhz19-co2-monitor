.headers off
.mode csv

SELECT 
	strftime('%H:%M', datetime(timestamp/1000, 'unixepoch'), 'localtime') as timestamp,
	co2
FROM Current;
