#pragma once

#include <array>

#include "config.h"

class Output;

class Monitor
{
public:
	Monitor();

	~Monitor();

	void set_output(Output *output);

	static void set_exit_handler();

	void loop();

protected:

private:
	Output *m_output = nullptr;

	void push(uint64_t ts, unsigned co2);

	bool check_sample_delta(size_t id) const;

	std::array<std::pair<uint64_t, unsigned>, SAMPLE_BUF_SIZE> m_buffer;

	size_t m_buffer_pos = 0;
};

