#pragma once

#include <string>
#include <random>

class SensorDummy
{
public:
	void open();

	void close();

	unsigned read();

protected:

private:
	inline static std::default_random_engine m_gen;
	inline static unsigned m_co2 = 0;
};
