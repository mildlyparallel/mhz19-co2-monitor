#pragma once

#include <config.h>

class SensorMHZ19 
{
public:
	SensorMHZ19();

	SensorMHZ19(const SensorMHZ19 &&other) = delete;

	SensorMHZ19(SensorMHZ19 &&other);

	~SensorMHZ19();

	void open();

	void close();

	bool opened() const;

	unsigned read();

protected:

private:
	static const constexpr char *c_device = MHZ19_DEVICE;
	static const constexpr unsigned c_retries = MAX_READ_RETRIES;

	void set_tty_attr();

	int m_fd = -1;
};
