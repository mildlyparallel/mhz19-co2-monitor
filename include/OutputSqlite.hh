#pragma once

#include "Output.hh"
#include "Sqlite.hh"

class OutputSqlite : public Output
{
public:
	OutputSqlite();

	virtual ~OutputSqlite();

	virtual void open(const std::string &uri);

	virtual void store(uint64_t ts, uint64_t co2);

	virtual void close();

	virtual void store_current(uint64_t ts, uint64_t co2);

protected:

private:
	Sqlite m_db;
};

