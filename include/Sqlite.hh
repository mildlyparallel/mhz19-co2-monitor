#pragma once

#include <string>
#include <tuple>
#include <string_view>
#include <sstream>
#include <functional>
#include <iostream>

struct sqlite3;

class SqliteStatement;

class Sqlite
{
public:
	Sqlite();

	virtual ~Sqlite();

	void open(const std::string &p);

	uint64_t get_rowid() const;

	static bool is_threadsafe();

	void close();

	void exec(const char *sql);

protected:

private:
	friend SqliteStatement;

	void init_pragmas();

	sqlite3 *get_handle();

	sqlite3 *m_handle = nullptr;
};

