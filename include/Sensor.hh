#pragma once

#ifdef DUMMY_SENSOR
#include "SensorDummy.hh"
using Sensor = SensorDummy;
#else
#include "SensorMHZ19.hh"
using Sensor = SensorMHZ19;
#endif

