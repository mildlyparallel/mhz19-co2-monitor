#pragma once

#include <string>

class Output
{
public:
	Output();

	virtual ~Output();

	virtual void open(const std::string &uri) = 0;

	virtual void store(uint64_t ts, uint64_t co2) = 0;

	virtual void close() = 0;

	virtual void store_current(uint64_t ts, uint64_t co2) = 0;

protected:
private:
};

