#pragma once

#include <string>
#include <tuple>
#include <string_view>

class Sqlite;
struct sqlite3;
struct sqlite3_stmt;

class SqliteStatement
{
public:
	static const constexpr int busy_retry_delay_ms = 10;

	SqliteStatement(Sqlite &db, const char *sql);

	SqliteStatement(const SqliteStatement &other) = delete;

	SqliteStatement(SqliteStatement &&other);

	void operator= (SqliteStatement &&other);

	~SqliteStatement();

	const char *name_at(size_t pos) const;

	const char *type_at(size_t pos) const;

	size_t nr_columns() const;

	bool load();

	bool step();

	void bind_to(size_t pos, const char *value);

	void bind_to(size_t pos, const std::string &value);

	void bind_to(size_t pos, const std::string_view &value);

	void bind_to(size_t pos, uint64_t value);

	void load_at(size_t pos, uint64_t &value);

	void load_at(size_t pos, std::string &value);

	void load_at(size_t pos, double &value);

private:
	sqlite3_stmt *m_stmt = nullptr;

	sqlite3 *m_handle = nullptr;
};


