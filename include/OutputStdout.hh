#pragma once

#include "Output.hh"

class OutputStdout : public Output
{
public:
	OutputStdout();

	virtual ~OutputStdout();

	virtual void open(const std::string &uri);

	virtual void store(uint64_t ts, uint64_t co2);

	virtual void close();

	virtual void store_current(uint64_t ts, uint64_t co2);

protected:

private:
};

