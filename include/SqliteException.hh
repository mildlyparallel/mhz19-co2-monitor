#pragma once

#include <stdexcept>

class SqliteException: public std::exception {
public:
	SqliteException(int err_code);

	const char* what() const noexcept override;

private:
	int m_code;
};
